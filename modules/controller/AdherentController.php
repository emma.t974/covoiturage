<?php
require 'modules/model/Adherent.php';
class AdherentController
{
	  
	private $adherents;

	public function __construct(){
		$this->adherents = new Adherent();
	}

	function get(){

		$data['adherent'] = $this->adherent->get();
		include 'modules/view/member/supprimerProfil.php';		
	}
	
	
	public function inscription()
	{
		$errors = array();

		  if(isset($_POST['submit'])) {


		  	if (empty($_POST['nom'])) {
		  		$msg = "Le nom doit être rempli";
		  	
		  	}

		  	if (empty($_POST['prenom'])) {
		  		$msg = "Le prenom doit être rempli";
		  	
		  	}
		  	
		  	if (empty($_POST['pseudo'])) {
		  		$msg = "Le pseudo doit être rempli";
		  	
		  	}

		  	if(empty($errors)){
		  		$inscription = $this->adherents->inscription($_POST);

		  		if ($inscription) {
		  			$msg  = "L'adherent ".$_POST['prenom']." ".$_POST['nom']." a été inscrit!";

		  		}

		  		else {
		  			$msg = "Inpossible d'inscrire l'Adherent";

		  		}

		  		//$this->index($msg); // Redirection vers l'index
		  		}
		  	}
		  	
        include('modules/view/member/inscription.php');
	}

	public function connexion(){

		if(@($_POST['submit'])){
			$cnx =  $this->adherents->connexion($_POST);

			if($cnx){
				$verifPwd = password_verify($_POST['mdp'],$cnx['mdp']);
				if($verifPwd){
					session_start();
					$_SESSION['id'] = $cnx['id_adh'];
					$_SESSION['status'] = $cnx['status'];
				  header("Location: index.php?ctrl=Accueil"); 

				}
				else
					$msg = "Mot de passe incorrect";
			}
			else
				$msg = "Identifiant/Mot de passe incorrect";
		}
        include('modules/view/member/connection.php');


	}

	public function deconnexion(){
		include('modules/view/member/deconnexion.php');
	}


	public function profil(){

  include('modules/view/member/profil.php');
      }



     public function modificationProfil(){
     	session_start();
		$id = $_SESSION['id'];
		//echo $id;
		$Adherant = $this->adherents->get($id);
		//var_dump($params);
		
        if(@($_POST['submit'])){

                $update = $this->adherents-> modificationProfil($_POST,$id);
                $msg = ($update) ? "L'Adherent ".$_POST['prenom']." ".$_POST['nom']." a été mise a jour !" : "Impossible de mettre à jour ";
                header("Location: index.php?ctrl=Accueil");
        }

        $adherent = $this->adherents->get($_SESSION['id']);
        if (!$adherent) {
            die('Page Not Found 404');    
		}
		session_write_close();
        include 'modules/view/member/modificationProfil.php';
    }




  //        public function supprimerProfil(){
  //    	session_start();
		// $id = $_SESSION['id'];
		// //echo $id;
		// $Adherant = $this->adherents->get($id);
		// //var_dump($params);
		
  //       if(@($_POST['submit'])){

  //               $delete = $this->adherents-> supprimerProfil($_POST,$id);
  //               $msg = ($delete) ? "L'Adherent ".$_POST['prenom']." ".$_POST['nom']." a été supprimer !" : "Impossible de supprimer l'adherent ";
  //       }

  //       $adherent = $this->adherents->get($_SESSION['id']);
  //       if (!$adherent) {
  //           die('Page Not Found 404');    
		// }
		// session_write_close();
  //       include 'modules/view/member/supprimerProfil.php';
  //   }


        public function  supprimerProfil()
    {
    	session_start();
		$id = $_SESSION['id'];
		
		//echo $id;
		$Adherant = $this->adherents->get($id);

    	if(@$_POST['submit']){

        $delete = $this->adherents-> supprimerProfil($id);
        if($delete){
        	session_destroy();
        	header("Location: index.php?ctrl=Accueil"); 
        	
        }
        else
        	$msg = "Impossible de supprimer l'adherent";
    }
    session_write_close();
        include 'modules/view/member/supprimerProfil.php';
    }





}


