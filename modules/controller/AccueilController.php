<?php
require 'modules/model/Ville.php';
require 'modules/model/Trajet.php';
class AccueilController
{
	private $ville;
	private $trajet;

	public function __construct(){
		$this->ville = new Ville();
		$this->trajet = new Trajet();
	}
	
	public function index()
	{
	$ville = $this->ville->get();
	$trajet = $this->trajet->getday();
        include('modules/view/general/index.php');
	}
}
