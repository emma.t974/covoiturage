<?php
require_once 'modules/model/Trajet.php';
require_once 'modules/model/Ville.php';

class TrajetController


{
	private $trajet;
	private $ville;

	function __construct(){
		$this->trajet = new trajet();
		$this->ville = new ville();
	}

	function get(){
		$data['trajet'] = $this->trajet->get();
		//$trajet = $this->trajet->get();
		include 'modules/view/trajet/trajet.php';		
	}

	function getAll(){
		$data['trajet'] = $this->trajet->getAll();

		include 'modules/view/trajet/alltrajet.php';
	}

	function GetTrajetId(){
    session_start();
		$id=$_GET['id_trajet'];
    
    if (isset($_SESSION['id'])) {
      //echo "test";
     $id_adh=$_GET['id_adh'];
     $data = $this->trajet->GetById($id);
     $donnee = $this->trajet->countNbReserv($id,$id_adh);
    }else{
      $data = $this->trajet->GetById($id);
    }
		session_write_close();
		include 'modules/view/trajet/trajetid.php';	
	}

	function add(){
	$ville = $this->ville->get();
		if (isset($_POST['submit'])) {
			session_start();
			$id = $_SESSION['id'];
			session_write_close();
			$add = $this->trajet->add($_POST,$id);
			header("Location: ?ctrl=Trajet&mth=get");
		}
		include 'modules/view/trajet/add.php';
	}


    function search(){

    	$search_trajet_data['search_trajet'] = $this->trajet->search($_POST);

    	include 'modules/view/trajet/search.php';
    }

    function reservation(){
    	session_start();	
    	$id_adh_Adherant = $_SESSION['id'];
    	$id_trajet = $_GET['id_trajet'];
    	$reservation = $this->trajet->reservation($id_trajet,$id_adh_Adherant);
    	if ($reservation) {
    		echo 'Réussite';
    		header("Location: ?ctrl=Trajet&mth=getReservation");
    	}
    	else{
    		echo 'Problème d\'ajout à ce trajet';
    	}
    }




 	// function supprimerTrajet()
    

  //   {
  //   	session_start();
		// $id = $_SESSION['id'];
		// $id_trajet = $_GET['id_trajet'];
	

  //       $delete = $this->trajet->supprimerTrajet($id_trajet);
  //       if($delete){
  //       	header("Location: ?ctrl=Trajet&mth=get"); 
        	
  //       }
  //       else
  //       	$msg = "Impossible de supprimer le trajet";

  //   session_write_close();
        
  //   }




 	function supprimerTrajet()
    

    {
    	session_start();
		$id = $_SESSION['id'];
		$id_trajet = $_GET['id_trajet'];
	


        $delete = $this->trajet->supprimerTrajet($id_trajet);
        header("Location: ?ctrl=Trajet&mth=get"); 
        session_write_close();
        
    }




        function modificationTrajet(){
     	session_start();


     	$ville = $this->ville->get();
     	$id = $_SESSION['id'];
		$id_trajet = $_GET['id_trajet'];

		$trajet = $this->trajet->GetById($id_trajet);
		session_write_close();

		
        if(isset($_POST['submit'])){

        		$update = $this->trajet->modificationTrajet($_POST,$id_trajet);
        		header("Location: ?ctrl=Trajet&mth=get"); 
		        //session_write_close();

		    }
        include 'modules/view/trajet/modificationTrajet.php';
    }



    function getReservation(){
      session_start();

		$data['trajet'] = $this->trajet->getReservation($_SESSION['id']);

    session_write_close();
    	
    include 'modules/view/trajet/mesReservation.php';


    }

    function annulationReservation(){

      $id_trajet = $_GET['id_trajet'];

      $annulation = $this->trajet->annulationReservation($id_trajet);

        header("Location: ?ctrl=Trajet&mth=getReservation");


    }




  //     function GetTrajetId(){
  //   $id=$_GET['id_trajet'];
  //   $data = $this->trajet->GetById($id);
  //   include 'modules/view/trajet/trajetid.php'; 
  // }








}



?>