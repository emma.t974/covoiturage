<?php
session_start();
?>
<!DOCTYPE HTML>
<html lang="fr">

	<head>
		<title>Covoiturage</title>

		<!-- PLUGGING CSS -->
		<link href="template/plugging/bootstrap/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
		<link href="template/plugging/toastr/build/toastr.min.css" rel="stylesheet" />	
		<link href="template/plugging/fontawesome/css/all.css" rel="stylesheet" />	
		<link rel="icon" type="image/png" href="template/img/web/icon.png" />
		<!-- PLUGGING JS -->
		<script src="template/plugging/jquery/jquery-3.3.1.min.js" /></script>
		<script src="template/plugging/toastr/build/toastr.min.js" /></script>
		<script src="template/plugging/bootstrap/js/bootstrap.min.js" /></script>
		<script>toastr.options.positionClass = 'toast-bottom-right';</script>
		<link rel="stylesheet" type="text/css" href="modules/templates/style.css">
	 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>

	<body>