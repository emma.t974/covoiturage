<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <img width="4%" src="image/logo.png">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item active">
        <a class="nav-link text-white bg-dark" id="Acceuil" href="index.php">Acceuil<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link text-white bg-dark" href="?ctrl=Trajet&mth=getAll"><i class="fas fa-map-signs"></i> Les trajets<span class="sr-only">(current)</span></a>
      </li>

<?php if (@$_SESSION['id']){ ?> 

      <li class="nav-item active">
        <a class="nav-link text-white bg-dark" href="?ctrl=Trajet&mth=get"><i class="fas fa-car"></i> Mes trajets<span class="sr-only">(current)</span></a>
      </li>


      <li class="nav-item active">
        <a class="nav-link text-white bg-dark" href="?ctrl=Trajet&mth=getReservation"><i class="far fa-list-alt"></i> Mes réservations<span class="sr-only">(current)</span></a>
      </li>

       <?php } ?>
      


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white bg-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user-circle"></i> Mon compte
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        <?php if (@$_SESSION['id']){ ?> 


<a class="dropdown-item" href="?ctrl=Adherent&mth=modificationProfil"><i class="fas fa-user-edit"></i> Modifier votre profil</a>

<a class="dropdown-item" href="?ctrl=Adherent&mth=supprimerProfil" style="color : #c38e12;"><b><i class="fas fa-times"></i> Supprimer votre profil</b></a>
   <div class="dropdown-divider"></div>
<a class="dropdown-item" href="?ctrl=Adherent&mth=deconnexion" style="color : red;"><b><i class="fas fa-sign-out-alt"></i> Déconnexion</a></b>
        <?php } else { ?>
          <a class="dropdown-item " href="?ctrl=Adherent&mth=inscription"><i class="fas fa-user-plus"></i> M'inscrire</a>
          <div class="dropdown-divider"></div>

          <a class="dropdown-item" href="?ctrl=Adherent&mth=connexion" style="color : green;"><b><i class="fas fa-sign-in-alt"></i> Connexion</a></b>

        <?php } ?>

          <div> 
      </li>

    </ul>
<!--     <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search">
      <button class="btn btn-primary my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
    </form> -->
  </div>
</nav>
