
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>
 <link rel="stylesheet" href="templates/style.css">



<body>
<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #profil{
    padding : 2%;
  }
</style>

<br>
 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Être vous sûre de supprimer ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       La suppression du compte entraîne la suppression de ses informations concernant les trajets
      </div>
      <div class="modal-footer">
          <form method="post" action="?ctrl=Adherent&mth=supprimerProfil">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
       <input type="submit" class="btn btn-danger" id="submit" name="submit" value="Oui, je confirme la suppresion de mon compte"></form>
      </div>
    </div>
  </div>
</div>

<section>
<div id="profil">
<h1 style="color : #c38e12;"> <i class="fas fa-user-minus"></i> SUPPRIMER VOTRE PROFIL</h1> <hr>


<?php if (@$_SESSION['id']){ ?>

  <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Prenom</th>
      <th scope="col">Nom</th>
      <th scope="col">Pseudo</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

      <tr>
        <td><?php echo $Adherant["prenom"]; ?></td>
        <td><?php echo $Adherant["nom"]; ?></td>
        <td><?php echo $Adherant["pseudo"]; ?></td>
        <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
<i class="fas fa-trash-alt"></i> Supprimer mon compte
</button></td>
    </tr> 

  </tbody>
</table>

<p style="color :red;">
<?php echo @$msg; ?> </p>

<?php }else{ ?>
<center>
  <p>Veuillez vous inscrire afin de proposer un trajet.</p>
  <a href="?ctrl=Adherent&mth=inscription" class="btn btn-primary">Inscription</a>
</center>
<?php } ?>


</div>
</section>



</body>


<?php include 'modules/templates/footer.php'?>



