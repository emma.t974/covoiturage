
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>
 <link rel="stylesheet" href="templates/style.css">


    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

<body>
<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>

<br>
 


<section>
<div id="cnx">
<h1 style="color : #c38e12;"><i class="fas fa-sign-in-alt"></i> CONNEXION</h1><hr>
<form  action="?ctrl=Adherent&mth=connexion" method="post">



  <div class="form-group">
    <label for="pseudo"><i class="fas fa-users"></i> Pseudo :</label>
    <input style="width: 40%;" type="text" class="form-control" id="pseudo" name="pseudo"  placeholder="Enter votre pseudo" required>
 
  </div>


  <div class="form-group">
    <label for="mdp"><i class="fas fa-key"></i> Mot de passe :</label>
    <input style="width: 40%;"type="password" class="form-control" id="mdp" name="mdp" placeholder="Enter votre mots de passe" required>
  </div>

<div class="form-group">
  <label for="recaptcha"> <div class="g-recaptcha" data-sitekey="6LcL150UAAAAANOjjzRs_y34zsBprFmWTcsFXCmS" data-callback="recaptchaCallback" data-expired-callback="recaptchaExpired"></div></label>
</div>



  <input type="submit" name="submit" id="submit" class="btn btn-success" value="Se connecter" disabled> <a href="?ctrl=Adherent&mth=inscription" class="btn btn-secondary"><i class="fas fa-user-plus"></i> M'inscrire</a>


</form><br>
<p style="color :red;">
<?php echo @$msg; ?> </p>
</div>

</section>



</body>
<script>
    function recaptchaCallback(){
      $('#submit').prop('disabled', false);
     }

  function recaptchaExpired(){
    $('#submit').prop('disabled', true);
  }
  </script>


<?php include 'modules/templates/footer.php'?>

