<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>
 <link rel="stylesheet" href="templates/style.css">


<body>
<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #section1{
    margin-left: 1%;
  }
  #section2{
    margin-left: 40%;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  h1{
    padding-left: 3%; 
    color : #c38e12;
  }
  section {
    padding: 5px;
  }
</style>

<div>
  <img id="image" height="350px" width="100%;" src="image/banniere.jpg">
</div>

<section>
  <div>
    <h1 align="center"><u><span style="color: orange">B</span>ienvenue sur le site Omen Transports de covoiturage</u></h1>

    <p style="margin-left: 1%;">Omen Transport vous permet de prendre contact facilement avec des personnes qui vont vers votre direction.<br>Pour voyager en covoiturage.<br>Faites du covoiturages et rentabiliser vos déplacements. </p>
    
    <div style="border-style: solid; width: 30%;background-color: white;margin-left: 69.2%;background-color: black;">
      <h4><span style="color: orange" >En proposant votre covoiturage :</span></h4>
        <ul>
          <li style="color: white">Vous aidez de nombreuses personnes sur leur mobilité</li>
          <li style="color: white">Vous réduisez vos frais de déplacement</li>
          <li style="color: white">Vous participez à la réduction de la pollution.</li>
          <li style="color: white">Vous désengorgez les centre villes.</li>
        </ul>
    </div>
              <br>    
  </div>
</section>


<section>
  <br>
<form action="?ctrl=Trajet&mth=search" method="post">
  <center>
 
    <div class="form-row" id="section2">
      <div class="form-group col-md-4">
        <label for="inputState"><i class="fas fa-car"></i> Vous partez de :</label>
        <select id="depart" name="depart" class="form-control">
        <option> -- </option>
        <?php
        foreach($ville as $key => $value){
          echo"<option value='".$value['id_ville']."'>".$value['nom_ville']."</option>";
        }
        ?>
          </select>
      </div>


      <div class="form-group col-md-4" id="destination">

        <label for="inputState"><i class="fas fa-car-side"></i> Vous arrivez à : </label>
        <select id="arrive" name="arrive" class="form-control">
        <option> -- </option>
        <?php
        foreach($ville as $key => $value){
          echo"<option value='".$value['id_ville']."'>".$value['nom_ville']."</option>";
        }
        ?>
          </select>
      </div>
    </div>
  </center>

  <center><input type="submit" class="btn btn-primary" value="Recherche"></center><br> 
</form>
</center>


</section>
<section>
  <p>
  <h1><i class="fas fa-sun"></i> Les trajets du jour</h1>
</p>
  <?php //var_dump($trajet); 
  if($trajet != false) { ?>
  <table class="table">
  <thead class="thead-dark">
    <tr>
        <th scope="col"><i class="fas fa-map-signs"></i> Départ</th>
      <th scope="col"><i class="fas fa-car-side"></i> Arrivée</th>
      <th scope="col"><i class="fas fa-sort-numeric-up"></i> Nombre de places</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Date</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Heure</th>
      <th scope="col"><i class="fas fa-map-signs"></i> Trajet</th>
    

    </tr>
  </thead>
  <tbody>
    <?php foreach ($trajet as $key => $value) {
     $date = date_create($value['date']);
      $heure = date_create($value['heur']); ?>
      <tr>
          <td><?php echo $value['debut']; ?></td>
          <td><?php echo $value['fin']; ?></td>
          <?php if($value['nb_places'] == 0) echo "<td><b style='color: red;'>Complet</b></td>"; else { ?>
          <td><?php echo $value['nb_places']; ?></td> <?php } ?>
          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
          <td><?php echo date_format($heure, 'H:i'); ?></td>
          <td><a href="?ctrl=Trajet&mth=GetTrajetId&id_adh=<?php echo @$_SESSION['id']; ?>&id_trajet=<?php echo $value['id_trajet'] ?>"  class="btn btn-info"><i class="fas fa-eye"></i> Voir</a></td>


    </tr>
  <?php } 
} else echo "Aucun trajet disponible aujourd'hui"; 
  ?>
  </tbody>
</table>
  </section>

</body>
<?php include 'modules/templates/footer.php'?>

<script src="modules/view/general/changeville.js" /></script>