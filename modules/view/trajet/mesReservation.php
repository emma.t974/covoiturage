
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>



<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>
<br>




 <section>
<div id="cnx">
<h1 style="color : #c38e12;"> <i class="fas fa-car-side"></i> Mes Réservations</h1><hr>
    <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"><i class="fas fa-map-signs"></i> Départ</th>
      <th scope="col"><i class="fas fa-car-side"></i> Arrivée</th>
      <th scope="col"><i class="fas fa-sort-numeric-up"></i> Nombre de places</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Date</th>
      <th scope="col"><i class="fas fa-hourglass-half"></i> Heure</th>
      <th scope="col"> Annulation</th>
      <th scope="col">Trajet</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data['trajet'] as $key => $value) { 
      $date = date_create($value['date']);
      $heure = date_create($value['heur']);
      ?>
      <tr>
          <td><?php echo $value['debut']; ?></td>
          <td><?php echo $value['fin']; ?></td>
          <?php if($value['nb_places'] == 0) echo "<td><b style='color:red;'>Complet</b></td>"; else { ?>
          <td><?php echo $value['nb_places'];  ?></td> <?php } ?>
          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
          <td><?php echo date_format($heure, 'H:i'); ?></td>
          <td><p><a href="?ctrl=trajet&mth=annulationReservation&id_trajet=<?php echo $value['id_trajet']; ?>" type="button" class="btn btn-outline-danger"><i class="fas fa-window-close"></i> Annulé ma réservation</a></p></td>
          <td><a href="?ctrl=Trajet&mth=GetTrajetId&id_adh=<?php echo $_SESSION['id']; ?>&id_trajet=<?php echo $value['id_trajet'] ?>"  class="btn btn-info"><i class="fas fa-eye"></i> Voir</a></td>
    </tr>
  <?php } ?>
  </tbody>
</table>
<center><p><a href="index.php" type="button" class="btn btn-outline-danger"><i class="fas fa-undo"></i> Retour à l'acceuil</a></p></center> <br>
</section>

<?php include 'modules/templates/footer.php'?>

