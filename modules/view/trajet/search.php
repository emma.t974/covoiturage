  
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>

<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>
<br>

<section>

<div id="cnx">

<h1 style="color : #c38e12;"> <i class="fas fa-car-side"></i> Recherche de trajet sur : <?php echo $_POST['depart']; ?> <i class="fas fa-angle-double-right"></i> <?php echo $_POST['arrive']; ?> </h1><hr>
  <table class="table">
  <thead class="thead-dark">
    <tr>

 
      <th scope="col"><i class="fas fa-map-signs"></i>Ville de Départ</th>
      <th scope="col"><i class="fas fa-car-side"></i>Ville d' Arrivée</th>
      <th scope="col"><i class="fas fa-sort-numeric-up"></i> Nombre de places</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Date</th>
      <th scope="col"><i class="fas fa-hourglass-half"></i> Heure</th>
      <th scope="col"><i class="fas fa-map-signs"></i> Réservé ce Trajet</th> 


    </tr>
  </thead>
  <tbody>
    <?php if($search_trajet_data['search_trajet']){ 
    foreach ($search_trajet_data['search_trajet'] as $key => $value) { 
      $date = date_create($value['date']);
      $heure = date_create($value['heur']); ?>
      <tr>
          <td><?php echo $value['debuts']; ?></td>
          <td><?php echo $value['fins']; ?></td>
          <td><?php echo $value['nb_places']; ?></td>

          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
          <td><?php echo date_format($heure, 'H:i'); ?></td>
          <td><a href="?ctrl=Trajet&mth=GetTrajetId&id_trajet=<?php echo $value['id_trajet'] ?>"  class="btn btn-success"><i class="fas fa-car-side"></i> Afficher le trajet</a></td>

          
    </tr>
  <?php }}
  else
  {
    echo "<td>Aucune ville trouvée</td>";
  }
  ?>


  </tbody>
</table>

<center><p><a href="index.php?ctrl=Accueil" type="button" class="btn btn-outline-danger"><i class="fas fa-undo"></i> Retour à l'accueil</a></p></center> <br>

</div>
</section>

<?php include 'modules/templates/footer.php'?>