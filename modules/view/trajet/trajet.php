
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>



<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>
<br>







<section>
<div id="cnx">

<h1 style="color : #c38e12;"> <i class="fas fa-car-side"></i> Mes trajets </h1><hr>
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"><i class="fas fa-map-signs"></i> Départ</th>
      <th scope="col"><i class="fas fa-car-side"></i> Arrivée</th>
      <th scope="col"><i class="fas fa-sort-numeric-up"></i> Nombre de places</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Date</th>
      <th scope="col"><i class="fas fa-hourglass-half"></i> Heure</th>
      <th scope="col"><i class="fas fa-cog"></i></th>
      <th scope="col"><i class="fas fa-trash-alt"></i></th>
      <th scope="col"><i class="fas fa-map-signs"></i></th> 

    </tr>
  </thead>
  <tbody>
  	<?php
  foreach ($data['trajet'] as $key => $value) {
      if($value['id_adh'] == $_SESSION['id']){

      $date = date_create($value['date']);
      $heure = date_create($value['heur']); ?>
    	<tr>


      		<td><?php echo $value['debut']; ?></td>
      		<td><?php echo $value['fin']; ?></td>
          <?php if($value['nb_places'] == 0) echo "<td><b style='color:red;'>Complet</b></td>"; else { ?>
          <td><?php echo $value['nb_places'];  ?></td> <?php } ?>
          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
          <td><?php echo date_format($heure, 'H:i'); ?></td>

<?php if($value['date'] >= date("Y-m-d")){ 
?>
<td><a href="?ctrl=Trajet&mth=modificationTrajet&id_trajet=<?php echo $value['id_trajet'] ?>"class=" btn btn-dark"><i class="fas fa-edit"></i></a></td>
<?php } else echo "<td></td>"; ?>

<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-trash-alt"></i></button></td>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Être vous sûre de supprimer ce trajet ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       La suppression du trajet entraîne la suppression de tout informations !
      </div>
      <div class="modal-footer">
          <form method="post" action="?ctrl=Trajet&mth=supprimerTrajet&id_trajet=<?php echo $value['id_trajet']; ?>">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
       <input type="submit" class="btn btn-danger" id="submit" name="submit" value="Oui, je confirme la suppresion de ce trajet"></form>
      </div>
    </div>
  </div>
</div>

<td><a href="?ctrl=Trajet&mth=GetTrajetId&id_adh=<?php echo $_SESSION['id'] ?>&id_trajet=<?php echo $value['id_trajet'] ?>" class="btn btn-info"><i class="fas fa-eye"></i> Voir</a></td>


    </tr>



	<?php } }
	?>
  </tbody>
</table>
<?php if (@$_SESSION['id']){ ?>
<center><a href="?ctrl=Trajet&mth=add" class="btn btn-success"><i class="fas fa-car-side"></i> Proposer un trajet</a></center>
<?php }else{ ?>
<center>
	<p>Si vous souhaitez proposer un trajet, veuillez vous connecter.</p>
	<a href="?ctrl=Adherent&mth=inscription" class="btn btn-secondary"><i class="far fa-plus-square"></i> M'inscrire</a>

  <a href="?ctrl=Adherent&mth=connexion" class="btn btn-warning"><i class="fas fa-sign-in-alt"></i> Me connecter</a>


</center>
<?php } ?>

<a href="?ctrl=Trajet&mth=getAll" style="font-size : 0.8em;">Voir tous les trajets</a>
</div>
</section>

<?php include 'modules/templates/footer.php'?>

