
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>
<center>


<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>
<br>





<section>
<div id="cnx">
<h1 style="color : #c38e12;"> <i class="fas fa-edit"></i> <i class="fas fa-car-side"></i>  Modifier ce trajet </h1><hr>

<form action="?ctrl=Trajet&mth=modificationTrajet&id_trajet= <?php echo $trajet["id_trajet"]; ?>" method="post">


  <div class="form-group" id = >
    <label for="exampleFormControlSelect1">Départ de * :</label>
    <select style="width: 20%;" class="form-control" id="depart" name="debut"  >
        <?php 
        foreach($ville as $key => $value){
            echo"<option value='".$value['id_ville']."' required>".$value['nom_ville']."</option>";
          }
        ?>
 
    </select>
  </div>

<?php // var_dump($trajet); ?>
<div class="form-group">
    <label for="exampleFormControlSelect2">Arrivée à * :</label>
    <select style="width: 20%;" class="form-control" id="arrive" name="fin">
        <?php 
        foreach($ville as $key => $value){
            echo"<option value='".$value['id_ville']."' required>".$value['nom_ville']."</option>";
          }
        ?>
     
    </select>
</div>

<div class="form-group"> 
  <label class="control-label" for="date">Date * :</label>
  <input style="width: 20%;" class="form-control" id="date" name="date" type="date" value="<?php echo $trajet['date']; ?>" required/>
</div>


<div class="form-group"> 
  <label class="control-label" for="heur">Heure * :</label>
  <input style="width: 20%;" class="form-control" id="date" name="heur"  type="time" value="<?php echo $trajet['heur']; ?>" required/>
</div>

<div class="form-group">
  <label for="exampleFormControlTextarea1">Nombres de places * :</label><br>
  <input style="width: 20%;" type="number" min="1" max="6" name="nb_places" id="nb_places" value="<?php echo $trajet['nb_places']; ?>"required/>
</div>




<p>(*) Champ obligatoire</p>

<center><button type="submit" name="submit" id="submit" class="btn btn-success"><i class="far fa-map"></i> Modifier ce trajet</button></center>

</form>
</center>
</div>
</section>
<script src="modules/view/general/changeville.js" /></script>

<script>
var ville=  <?php echo json_encode($trajet['idville1']); ?>;
var ville2= <?php echo json_encode($trajet['idville2']);?>;

console.log(ville);
console.log(ville2);

$('select[id=depart] option[value='+ville+']').prop('selected',true);

$('select[id=arrive] option[value='+ville2+']').prop('selected',true);

</script>
<?php include 'modules/templates/footer.php'?>
