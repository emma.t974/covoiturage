
<?php include("modules/templates/header.php") ?>
<?php include("modules/templates/nav-top.php")?>
    <link rel="stylesheet" href="template/plugging/openlayers/v5.3.0/css/ol.css" type="text/css">
    <script src="template/plugging/openlayers/v5.3.0/build/ol.js"></script>


<style>
  body{
    background-image: url(image/fond3.jpg);
    background-size:cover;
  }
  #destination{
    margin-right: 25em;
  }
  #image{
    margin-top: -1%;
  }
  #cnx{

    padding: 2%;
  }
</style>
<body onload="javascript:addMarker(<?php echo ($data['debutLong'].",".$data['debutLat'].",".$data['finLong'].",".$data["finLat"]) ?>)">
<br>
<?php
//Formé les dates
//var_dump($data);
$date = date_create($data['date']);
$heure = date_create($data['heur']);
?>
<?php 
// $adherent = 0;

// foreach($adh as $key => $value){
//   if($value['id_adh'] == $_SESSION['id']){
//     $adherent = $value['id_adh'];
//   } 
// }
//var_dump($donnee[0]['nb_reservation']); ?>
<section>
<div id="cnx">
<h1 style="color : #c38e12;"> <i class="fas fa-car-side"></i> Trajet de <?php echo $data['debut']; ?> <i class="fas fa-angle-double-right"></i> <?php echo $data['fin']; ?> </h1><hr>
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"><i class="fas fa-map-signs"></i> Départ</th>
      <th scope="col"> <i class="fas fa-car-side"></i> Arrivée</th>
      <th scope="col"><i class="fas fa-sort-numeric-up"></i> Nombre de places</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Date</th>
      <th scope="col"><i class="fas fa-hourglass-half"></i>Heure</th>
      <th scope="col"><i class="fas fa-calendar-week"></i> Résevation</th>
    </tr>
  </thead>
  <tbody>
 	<tr>
      		<td><?php echo $data['debut']; ?></td>
      		<td><?php echo $data['fin']; ?></td>
          <?php if($data['nb_places'] == 0) echo "<td><b style='color:red;'>Complet</b></td>"; else { ?>
          <td><?php echo $data['nb_places'];  ?></td> <?php } ?>
          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
          <td><?php echo date_format($heure, 'H:i'); ?></td>
          <?php if(@$_SESSION['id'] != ""){
            ?>
             <?php if($data['nb_places'] > 0 && $data['date'] >= date("Y-m-d") && $_SESSION['id'] != $data['id_adh'] && $donnee[0]['nb_reservation'] == 0) { 
            ?>
          <td><a href="?ctrl=Trajet&mth=reservation&id_adh=<?php echo $data['id_adh'] ?>&id_trajet=<?php echo $data['id_trajet'] ?>" class="btn btn-success"><i class="fas fa-check"></i> Réserver</a></td></tr>
        <?php } else { 
          ?>
          <td><button class="btn btn-danger" disabled><i class="fas fa-times"></i> Indisponible </button> </td></tr> <?php } } else echo "<td>Vous devrez être connecter pour réserver</td>"; ?>
    </tr>
  </tbody>
</table>


    <div class="container" id="liste_covoiturage">
      
      <div class="col">
        <div id="map" class="map"></div>

          <script type="text/javascript">
            var map = new ol.Map({
              target: 'map',
              layers: [
                new ol.layer.Tile({
                  source: new ol.source.OSM()
                })
              ],
              view: new ol.View({
                center: ol.proj.fromLonLat([55.536384,-21.115142]),
                zoom: 10.5
              })
            });

            addMarker();

            function addMarker(){
              var lonA = <?php echo $data['debutLong']; ?>;
              var latA = <?php echo $data['debutLat']; ?>;
              var lonB = <?php echo $data['finLong']; ?>;
              var latB = <?php echo $data['finLat']; ?>;

              var markerA = new ol.Feature({
            geometry: new ol.geom.Point(
              ol.proj.fromLonLat([lonA,latA])
            ),
          });
          var vectorSourceA = new ol.source.Vector({
            features: [markerA]
          });
          var markerVectorLayerA = new ol.layer.Vector({
            source: vectorSourceA,
          });
          map.addLayer(markerVectorLayerA);

          remove();

          var vectorLayerA = new ol.layer.Vector({
          source:new ol.source.Vector({
          features: [new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lonA), parseFloat(latA)], 'EPSG:4326', 'EPSG:3857')),
          })]
          }),
          style: new ol.style.Style({
          image: new ol.style.Icon({
          scale: 0.15,
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "image/marker.png"
          })
          })
          });
          map.addLayer(vectorLayerA);

          var vectorLayerB = new ol.layer.Vector({
          source:new ol.source.Vector({
          features: [new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lonB), parseFloat(latB)], 'EPSG:4326', 'EPSG:3857')),
          })]
          }),
          style: new ol.style.Style({
            image: new ol.style.Icon({
            scale: 0.15,
            anchor: [0.5, 0.5],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "image/marker.png"
            })
          })
          });
          map.addLayer(vectorLayerB);

          var firstExtend = vectorLayerA.getDataExtent();
          var secondExtend = vectorLayerB.getDataExtent();

          var totalBounds = firstExtent.extend(secondExtent);

          map.zoomToExtent(totalBounds);
            }

            function remove(){
              map.getLayers().forEach(function(layer) {
                if(layer instanceof ol.layer.Vector){
                  map.removeLayer(layer);
                }
              });

              map.getLayers().forEach(function(layer) {
                if(layer instanceof ol.layer.Vector){
                  map.removeLayer(layer);
                }
              });
            }
          </script>
      </div>
    </div>
  </div>
  <?php if (@$_SESSION['id']){ ?> 
 <center><p><a href="?ctrl=Trajet&mth=get" type="button" class="btn btn-outline-danger"><i class="fas fa-undo"></i> Retour au trajet</a></p></center> <br>
    <?php } else { ?>

<center><p><a href="index.php?ctrl=Accueil" type="button" class="btn btn-outline-danger"><i class="fas fa-undo"></i> Retour à l'accueil</a></p></center> <br>

    <?php } ?>
</body>
</html>
</section>

<?php include 'modules/templates/footer.php'?>

