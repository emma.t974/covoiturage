<?php
/**
 * By EMMA TONY GEORGES
 **/
require_once "ConnexionDB.php";
class Adherent extends ConnexionDB{

    //Récupération d'un Adherent
    public function get($id){
        $req = $this->cnx->prepare("SELECT nom, prenom, pseudo, status FROM Adherant WHERE id_adh=?");
        $req->execute(array($id));

        return $req->fetch();
    }

    //Récupération de tous les adherents
    public function getAll(){
        $req = $this->cnx->prepare("SELECT nom, prenom, pseudo, status FROM Adherant");
        $req->execute();

        return $req->fetchAll();
    }

    //Inscription

    public function inscription($post){
        $pwd = password_hash($post['mdp'], PASSWORD_DEFAULT);

        $req = $this->cnx->prepare("INSERT INTO Adherant (nom, prenom, pseudo, mdp) VALUES(?,?,?,?)");

        $req->execute(array($post['nom'],$post['prenom'],$post['pseudo'],$pwd));

        return $req->rowCount();
    }

    //Connection
    public function connexion($post){
        $sql = $this->cnx->prepare("SELECT id_adh, status, mdp FROM Adherant WHERE pseudo like ?");
        $sql->execute(array($post['pseudo']));

        return $result = $sql->fetch();
    }



        public function modificationProfil($post,$id)
    {
        $sql = $this->cnx->prepare("UPDATE Adherant 
                                    SET nom=?,prenom=?,pseudo=?
                                    WHERE id_adh=?");
        $sql->execute( array($post['nom'],$post['prenom'],$post['pseudo'],$id) );
        return $sql->rowCount();
    }


       public function supprimerProfil($id)


    { 
        $sql = $this->cnx->prepare("DELETE FROM est_passage WHERE id_adh=?");
        $sql->execute(array($id));
        // return $sql->rowCount();


        $sql = $this->cnx->prepare("DELETE FROM Propose WHERE id_adh=?");
        $sql->execute(array($id));
        // return $sql->rowCount();


        $sql = $this->cnx->prepare("DELETE FROM Trajet WHERE id_adh=?");
        $sql->execute(array($id));
        // return $sql->rowCount();


        $sql = $this->cnx->prepare("DELETE FROM Adherant WHERE id_adh=?");
        $sql->execute(array($id));
        return $sql->rowCount();


    }





    
}