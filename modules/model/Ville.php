<?php
require_once "ConnexionDB.php";
class Ville extends ConnexionDB{

    public function get(){
        $req = $this->cnx->prepare("SELECT * FROM Ville");
        $req->execute();

        return $req->fetchAll();
    }
}