<?php
/**
 * 
 */
require_once "ConnexionDB.php";

class Trajet extends ConnexionDB{

	public function GetById($id){
			$req =  $this->cnx->query("SELECT distinct Ville.latitude debutLat,Ville.id_ville idville1,Ville.longitude debutLong,Ville2.id_ville idville2 ,Ville2.latitude finLat,Ville2.longitude finLong,id_trajet,Ville.nom_ville debut,Ville2.nom_ville fin,nb_places, date, id_adh, heur
									FROM Trajet,Ville,Ville Ville2 
									WHERE Trajet.debut=Ville.id_ville 
									AND Trajet.fin=Ville2.id_ville 
                                    AND id_trajet = $id");
			return $req->fetch();
	}

	public function countNbReserv($id,$id_adh){
		$sql = $this->cnx->prepare("SELECT count(*) as nb_reservation 
										FROM est_passage 
										WHERE id_adh=$id_adh 
										AND id_trajet=$id");			
			$sql->execute(array($id_adh,$id));
			return $sql->fetch();
	}
	
	public function get()

	{

		return $this->cnx->query("SELECT distinct id_trajet,Ville.nom_ville debut,Ville2.nom_ville fin,nb_places, date, heur, Trajet.id_adh
									FROM Trajet,Ville,Ville Ville2, Adherant
									WHERE Trajet.debut=Ville.id_ville 
									AND Trajet.fin=Ville2.id_ville
                                    order by date desc");
	}

	public function getday(){
		$req = $this->cnx->prepare("SELECT id_trajet,Ville.nom_ville debut,Ville2.nom_ville fin,nb_places, date, heur
									FROM Trajet,Ville,Ville Ville2 
									WHERE Trajet.debut=Ville.id_ville 
									AND Trajet.fin=Ville2.id_ville 
									AND date = CURDATE()");

		$req->execute();

		return $req->fetchAll();
	}

		public function getAll(){
		return $this->cnx->query("SELECT id_trajet,Ville.nom_ville debut,Ville2.nom_ville fin,nb_places, date, heur
									FROM Trajet,Ville,Ville Ville2 
									WHERE Trajet.debut=Ville.id_ville 
									AND Trajet.fin=Ville2.id_ville 
									AND CONCAT(date,' ',heur) >= NOW()
                                  	order by date desc");
	}

	public function getest_passage($id){
		$req = $this->cnx->prepare("SELECT id_adh FROM est_passage where id_trajet=?");
		$req->execute(array($id));

		return $req->fetchAll();
	}


	public function add($post,$id){
		$sql = $this->cnx->prepare("  INSERT INTO Trajet (debut,fin,nb_places,date,heur,id_adh) VALUES (?,?,?,?,?,?) ");


		$sql->execute(array($post['debut'],$post['fin'],$post['nb_places'],$post['date'],$post['heur'],$id));
		$last_id = $this->cnx->lastInsertId();

		$req = $this->cnx->prepare("INSERT INTO Propose(id_trajet, id_adh) VALUES (?,?)");
		$req->execute(array($last_id,$id));

		return $sql->rowCount();
	}

	public function reservation($id_trajet,$id_adh_Adherant){
		$sql = $this->cnx->prepare("INSERT INTO est_passage (id_trajet,id_adh) 
									VALUES ($id_trajet,$id_adh_Adherant)");

		$sql->execute(array($id_trajet,$id_adh_Adherant));

		$req = $this->cnx->prepare("UPDATE Trajet SET nb_places = nb_places - 1 where id_trajet=$id_trajet");
		$req->execute();

		return $sql->rowCount();
	}



   public function search($post){
        $req = $this->cnx->prepare("SELECT distinct id_trajet,Ville.nom_ville debuts,Ville2.nom_ville fins,nb_places, date, heur
									FROM Trajet,Ville,Ville Ville2 
									WHERE Trajet.debut=Ville.id_ville 
									AND Trajet.fin=Ville2.id_ville  
									AND debut = ?
									AND fin = ?
									AND CONCAT(date,' ',heur) >= NOW()
									order by date desc");
        $req->execute(array($post['depart'],$post['arrive']));

        return $req->fetchAll();
    }





   public function supprimerTrajet($id_trajet){
   		$req = $this->cnx->prepare("DELETE FROM est_passage WHERE id_trajet = $id_trajet");
   		$req->execute();
   		unset($req);

   		$req = $this->cnx->prepare("DELETE FROM Propose WHERE id_trajet = $id_trajet");
   		$req->execute();


        $sql = $this->cnx->prepare("DELETE FROM Trajet WHERE id_trajet=$id_trajet");
        $sql->execute(array($id_trajet));
}




   public function modificationTrajet($post,$id_trajet)
    
    {

        $sql = $this->cnx->prepare("UPDATE Trajet 
                                    SET debut=?,fin=?, nb_places=?, date=?, heur=?
                                    WHERE id_trajet=$id_trajet");
        $sql->execute(array($post['debut'],$post['fin'],$post['nb_places'],$post['date'],$post['heur']));
        return $sql->rowCount();
    }



    public function getReservation($id_adh)

    {

	$req = $this->cnx->prepare("SELECT distinct est_passage.id_trajet,Ville.nom_ville debut,Ville2.nom_ville fin,nb_places, date, est_passage.id_adh, heur
	FROM Trajet,Ville,Ville Ville2, est_passage
	WHERE Trajet.debut=Ville.id_ville 
	AND Trajet.fin=Ville2.id_ville 
	AND Trajet.id_trajet = est_passage.id_trajet
	AND est_passage.id_adh = $id_adh");

    $req->execute(array($id_adh));

    return $req->fetchAll();

    }

    public function annulationReservation($id_trajet){
    	$sql = $this->cnx->prepare("DELETE FROM est_passage WHERE id_trajet=$id_trajet");
    	$sql->execute(array($id_trajet));

    	$req = $this->cnx->prepare("UPDATE Trajet SET nb_places = nb_places + 1 where id_trajet=$id_trajet");
		$req->execute();

    }



 


}